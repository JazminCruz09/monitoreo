SELECT	acc.Codigo, 
		acc.Message, 
		ISNULL(dts.ID_USUARIO, 0) AS IdUsuario,
		ISNULL(dts.Nombre, '') AS Nombre,
		ISNULL(dts.APELLIDO_PATERNO, '') AS ApellidoPaterno,
		ISNULL(dts.APELLIDO_MATERNO, '') AS ApellidoMaterno,
		ISNULL(dts.NUM_EMPLEADO, '') AS NumEmpleado,
		ISNULL(dts.CORREO, '') AS Correo,
		ISNULL(dts.IMAGEN, '') AS Imagen,
		ISNULL(dts.ID_ROL, 0) AS Rol,
		ISNULL(dts.DESC_ROL, '') AS Perfil
FROM
(
	SELECT	CASE WHEN (COUNT(PASSWORD) = 1) AND (COUNT(NUM_EMPLEADO) = 1) THEN 0 ELSE 1 END Codigo,
			CASE WHEN (COUNT(PASSWORD) = 1) AND (COUNT(NUM_EMPLEADO) = 1) THEN 'Acceso Correcto' ELSE 'Datos incorrectos' END Message
	  FROM	schvtc_raw_sie_monitor.RAW_CAT_USUARIO us
	 WHERE	us.NUM_EMPLEADO = :PcNumEmp
	   AND	us.PASSWORD = :PcPassword
	   AND	us.STATUS = TRUE 
	 LIMIT	1
) AS acc
LEFT JOIN
(
	SELECT	0 AS Codigo,
			us.ID_USUARIO,
			us.NOMBRE,
			us.APELLIDO_PATERNO,
			us.APELLIDO_MATERNO,
			us.NUM_EMPLEADO,
			us.CORREO,
			us.IMAGEN,
			pr.ID_ROL,
			r.DESC_ROL 
	  FROM	schvtc_raw_sie_monitor.RAW_CAT_USUARIO us
INNER JOIN	schvtc_raw_sie_monitor.RAW_DET_PERFILAMIENTO pr
		ON	pr.ID_USUARIO = us.ID_USUARIO 
INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ROL r 
		ON	r.ID_ROL = pr.ID_ROL
	 WHERE	us.NUM_EMPLEADO = :PcNumEmp
	   AND	us.PASSWORD = :PcPassword
	   AND	us.STATUS = TRUE 
	 LIMIT	1
) AS dts
ON acc.Codigo = dts.Codigo;