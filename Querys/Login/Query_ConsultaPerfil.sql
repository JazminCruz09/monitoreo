		SELECT	p.ID_USUARIO	AS IdUsuario,
				u.NOMBRE || ' ' || u.APELLIDO_PATERNO || ' ' || u.APELLIDO_MATERNO AS Usuario,
				p.ID_ROL 		AS IdRol,
				r.DESC_ROL		AS Rol,
				p.ID_ACCESO 	AS IdAcceso,
				a.DESC_ACCESO	AS Acceso,
				p.CONSULTAR		AS Consultar,
				p.AGREGAR		AS Agregar,
				p.EDITAR		AS Editar,
				p.ELIMINAR		AS Eliminar,
				p.DESCARGAR		AS Descargar
		FROM	schvtc_raw_sie_monitor.RAW_DET_PERFILAMIENTO p
  INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_USUARIO u
		  ON	p.ID_USUARIO = u.ID_USUARIO 
  INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ROL r
		  ON	r.ID_ROL = p.ID_ROL 
  INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ACCESO a 
		  ON	a.ID_ACCESO = p.ID_ACCESO
	   WHERE	1=1
	  	 AND	p.ID_USUARIO = :PiUsuario 
