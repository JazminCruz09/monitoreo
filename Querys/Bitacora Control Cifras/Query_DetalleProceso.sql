SELECT	a.IdDetalleCifra
		,a.Fecha
		,a.IdSubproceso
		,UPPER(a.Subproceso) AS Subproceso
		,a.IdMetrica
		,a.Metrica
		,b.HoraInicio
		,ISNULL(c.HoraFin, '') AS HoraFin 
		,ISNULL(d.MontoInicial, 0) AS MontoInicial
		,CASE WHEN ISNULL(c.HoraFin, '') =  '' THEN 0 ELSE ISNULL(e.MontoFinal, 0.0) END AS MontoFinal 
		,ISNULL(f.ConteoInicial, 0) AS ConteoInicial
		,ISNULL(g.ConteoFinal, 0) AS ConteoFinal
		,a.Comentarios
		
FROM (
		SELECT	dc.ID_DETALLE_CIFRA AS IdDetalleCifra, 
				cc.FECHA::DATE 		AS Fecha,
				dc.ID_SUBPROCESO 	AS IdSubproceso,
				s.SUBPROCESO 		AS Subproceso,
				dc.ID_METRICA 		AS IdMetrica,
				m.METRICA 			AS Metrica,
				cc.NOTAS 			AS Comentarios
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e 
			ON	dc.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s
			ON	dc.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dc.ID_METRICA = m.ID_METRICA AND dc.ID_INDICADOR = m.ID_INDICADOR 
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	dc.ID_INDICADOR = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
		   AND	dc.ID_ETAPA = :PiEtapa
	  GROUP BY	dc.ID_DETALLE_CIFRA,
				cc.FECHA::DATE,
				dc.ID_SUBPROCESO,
				s.SUBPROCESO,
				dc.ID_METRICA,
				m.METRICA,
				cc.NOTAS 
)AS A
LEFT JOIN 
(
			
		SELECT	dc.ID_DETALLE_CIFRA AS IdDetalleCifra, 
				cc.FECHA::DATE 		AS Fecha,
				dc.ID_SUBPROCESO 	AS IdSubproceso,
				dc.ID_METRICA 		AS IdMetrica,
				MIN(cc.FECHA)::TIME::VARCHAR(8) AS HoraInicio
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e 
			ON	dc.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s
			ON	dc.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dc.ID_METRICA = m.ID_METRICA AND dc.ID_INDICADOR = m.ID_INDICADOR 
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	dc.ID_INDICADOR = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
		   AND	dc.ID_ETAPA = :PiEtapa
	  GROUP BY	dc.ID_DETALLE_CIFRA,
				cc.FECHA::DATE,
				dc.ID_SUBPROCESO,
				dc.ID_METRICA
)AS B
ON A.IdDetalleCifra = B.IdDetalleCifra
LEFT JOIN 
(
		SELECT	dc.ID_DETALLE_CIFRA AS IdDetalleCifra, 
				cc.FECHA::DATE 		AS Fecha,
				dc.ID_SUBPROCESO 	AS IdSubproceso,
				dc.ID_METRICA 		AS IdMetrica,
				MAX(cc.FECHA)::TIME::VARCHAR(8) AS Horafin
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e 
			ON	dc.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s
			ON	dc.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dc.ID_METRICA = m.ID_METRICA AND dc.ID_INDICADOR = m.ID_INDICADOR 
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	dc.ID_INDICADOR = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
		   AND	dc.ID_ETAPA = :PiEtapa
	  GROUP BY	dc.ID_DETALLE_CIFRA,
				cc.FECHA::DATE,
				dc.ID_SUBPROCESO,
				dc.ID_METRICA
)AS C
ON A.IdDetalleCifra = C.IdDetalleCifra
LEFT JOIN 
(
		SELECT	dc.ID_DETALLE_CIFRA AS IdDetalleCifra,
				cc.FECHA::DATE 		AS Fecha,
				dc.ID_SUBPROCESO 	AS IdSubproceso,
				dc.ID_METRICA 		AS IdMetrica,
				ISNULL(SUM(cc.TOTAL_CIFRA), 0) AS MontoInicial
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e 
			ON	dc.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s
			ON	dc.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dc.ID_METRICA = m.ID_METRICA AND dc.ID_INDICADOR = m.ID_INDICADOR 
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	dc.ID_INDICADOR = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
		   AND	cc.FECHA::TIME IN (
		   							SELECT	MIN(FECHA)::TIME
		   							FROM	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS 
		   							WHERE	ID_DETALLE_CIFRA = dc.ID_DETALLE_CIFRA 
		   							AND 	FECHA::DATE = :PdFecha
		   						  )
		   AND	dc.ID_ETAPA = :PiEtapa
		   AND	dc.ID_TIPO_CIFRA = 2
	  GROUP BY	dc.ID_DETALLE_CIFRA,
				cc.FECHA::DATE,
				dc.ID_SUBPROCESO,
				dc.ID_METRICA
)AS D
ON A.IdDetalleCifra = D.IdDetalleCifra
LEFT JOIN
(
		SELECT	dc.ID_DETALLE_CIFRA AS IdDetalleCifra, 
				cc.FECHA::DATE 		AS Fecha,
				dc.ID_SUBPROCESO 	AS IdSubproceso,
				dc.ID_METRICA 		AS IdMetrica,
				ISNULL(SUM(cc.TOTAL_CIFRA), 0) AS MontoFinal
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e 
			ON	dc.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s
			ON	dc.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dc.ID_METRICA = m.ID_METRICA AND dc.ID_INDICADOR = m.ID_INDICADOR 
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	dc.ID_INDICADOR = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
		   AND	cc.FECHA::TIME IN (
		   							SELECT	MAX(FECHA)::TIME
		   							FROM	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS 
		   							WHERE	ID_DETALLE_CIFRA = dc.ID_DETALLE_CIFRA 
		   							AND 	FECHA::DATE = :PdFecha
		   						  )
		   AND	dc.ID_ETAPA = :PiEtapa
		   AND	dc.ID_TIPO_CIFRA = 2
	  GROUP BY	dc.ID_DETALLE_CIFRA,
				cc.FECHA::DATE,
				dc.ID_SUBPROCESO,
				dc.ID_METRICA
)AS E
ON A.IdDetalleCifra = E.IdDetalleCifra
LEFT JOIN 
(
		SELECT	dc.ID_DETALLE_CIFRA AS IdDetalleCifra,
				cc.FECHA::DATE 		AS Fecha,
				dc.ID_SUBPROCESO 	AS IdSubproceso,
				dc.ID_METRICA 		AS IdMetrica,
				ISNULL(SUM(cc.TOTAL_CIFRA), 0) AS ConteoInicial
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e 
			ON	dc.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s
			ON	dc.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dc.ID_METRICA = m.ID_METRICA AND dc.ID_INDICADOR = m.ID_INDICADOR 
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	dc.ID_INDICADOR = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
		   AND	cc.FECHA::TIME IN (
		   							SELECT	MIN(FECHA)::TIME
		   							FROM	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS 
		   							WHERE	ID_DETALLE_CIFRA = dc.ID_DETALLE_CIFRA 
		   							AND 	FECHA::DATE = :PdFecha
		   						 )
		   AND	dc.ID_ETAPA = :PiEtapa
		   AND	dc.ID_TIPO_CIFRA = 1
	  GROUP BY	dc.ID_DETALLE_CIFRA,
				cc.FECHA::DATE,
				dc.ID_SUBPROCESO,
				dc.ID_METRICA
)AS F
ON A.IdDetalleCifra = F.IdDetalleCifra
LEFT JOIN
(
		SELECT	dc.ID_DETALLE_CIFRA AS IdDetalleCifra, 
				cc.FECHA::DATE 		AS Fecha,
				dc.ID_SUBPROCESO 	AS IdSubproceso,
				dc.ID_METRICA 		AS IdMetrica,
				ISNULL(SUM(cc.TOTAL_CIFRA), 0) AS ConteoFinal
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e 
			ON	dc.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s
			ON	dc.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dc.ID_METRICA = m.ID_METRICA AND dc.ID_INDICADOR = m.ID_INDICADOR 
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	dc.ID_INDICADOR = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
		   AND	cc.FECHA::TIME IN (
		   							SELECT	MAX(FECHA)::TIME
		   							FROM	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS 
		   							WHERE	ID_DETALLE_CIFRA = dc.ID_DETALLE_CIFRA 
		   							AND 	FECHA::DATE = :PdFecha
		   						 )
		   AND	dc.ID_ETAPA = :PiEtapa
		   AND	dc.ID_TIPO_CIFRA = 1
	  GROUP BY	dc.ID_DETALLE_CIFRA,
				cc.FECHA::DATE,
				dc.ID_SUBPROCESO,
				dc.ID_METRICA
)AS G
ON A.IdDetalleCifra = G.IdDetalleCifra
GROUP BY	A.IdDetalleCifra, 
			A.Fecha,
			A.IdSubproceso,
			A.Subproceso,
			a.IdMetrica,
			a.Metrica,
			b.HoraInicio
			,c.HoraFin
			,D.MontoInicial
			,E.MontoFinal
			,F.ConteoInicial
			,g.ConteoFinal
			,a.Comentarios
ORDER BY A.IdSubproceso, A.IdMetrica;
