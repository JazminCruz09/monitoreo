SELECT	A.IdEtapa,
		A.Etapa,
		B.HoraInicio,
		C.HoraFin 
FROM (
		SELECT	dt.ID_ETAPA AS IdEtapa
				,e.ETAPA AS Etapa
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dt
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e
			ON	dt.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s 
			ON	dt.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dt.ID_METRICA = m.ID_METRICA AND dt.ID_INDICADOR = m.ID_INDICADOR
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc
			ON	dt.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	cc.ID_DETALLE_CIFRA = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
	 GROUP BY	dt.ID_ETAPA
				,e.ETAPA
)AS A
INNER JOIN 
(
		SELECT	cc.FECHA::DATE AS Fecha,
				dt.ID_ETAPA 
				AS IdEtapa,
				MIN(cc.FECHA)::TIME::VARCHAR(8) AS HoraInicio
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dt
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e
			ON	dt.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s 
			ON	dt.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dt.ID_METRICA = m.ID_METRICA AND dt.ID_INDICADOR = m.ID_INDICADOR
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc
			ON	dt.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	cc.ID_DETALLE_CIFRA = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
	  GROUP BY	cc.FECHA::DATE
				,dt.ID_ETAPA
)AS B
ON A.IdEtapa  = B.IdEtapa
INNER JOIN 
(
		SELECT	cc.FECHA::DATE AS Fecha
				,dt.ID_ETAPA 
				AS IdEtapa,
				MAX(cc.FECHA)::TIME::VARCHAR(8) AS HoraFin
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dt
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_ETAPA e
			ON	dt.ID_ETAPA = e.ID_ETAPA 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_SUBPROCESO s 
			ON	dt.ID_SUBPROCESO = s.ID_SUBPROCESO 
	INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_METRICA m 
			ON	dt.ID_METRICA = m.ID_METRICA AND dt.ID_INDICADOR = m.ID_INDICADOR
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc
			ON	dt.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	cc.ID_DETALLE_CIFRA = :PiIndicador
		   AND	cc.FECHA::DATE = :PdFecha
	  GROUP BY	cc.FECHA::DATE
				,dt.ID_ETAPA
)AS C
ON A.IdEtapa  = C.IdEtapa
ORDER BY A.IdEtapa;


