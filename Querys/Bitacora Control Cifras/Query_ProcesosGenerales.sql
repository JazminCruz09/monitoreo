SELECT	A.IdIndicador,
		A.Indicador,
		ISNULL(B.HoraInicio, '') AS HoraInicio,
		ISNULL(C.HoraFin, '') AS HoraFin,
		'' AS Estatus,
		0 AS Origen
FROM 
	(
		SELECT	ID_INDICADOR AS IdIndicador,
				UPPER(INDICADOR) AS Indicador
		  FROM	schvtc_raw_sie_monitor.RAW_CAT_INDICADORMONITOREO
	)AS A
LEFT JOIN 
	(
		SELECT	dc.ID_INDICADOR AS IdIndicador,
				MIN(cc.FECHA)::TIME::VARCHAR(8) AS HoraInicio
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA
		 WHERE	cc.FECHA::DATE = :PdFecha
		   AND	dc.ID_ETAPA = (
								SELECT	MIN(ID_ETAPA)
								  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA
								 WHERE	ID_INDICADOR = dc.ID_INDICADOR)
	  GROUP BY	dc.ID_INDICADOR
	)AS B
	ON A.IdIndicador = B.IdIndicador
LEFT JOIN 
	(
		SELECT	dc.ID_INDICADOR AS IdIndicador,
				MAX(cc.FECHA)::TIME::VARCHAR(8) AS HoraFin
		  FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA dc
	 LEFT JOIN	schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS cc 
			ON	dc.ID_DETALLE_CIFRA = cc.ID_DETALLE_CIFRA 
		 WHERE	cc.FECHA::DATE = :PdFecha
		   AND	dc.ID_ETAPA = (
		   						SELECT	MAX(ID_ETAPA) 
		   						 FROM	schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA 
		   						WHERE	ID_INDICADOR = dc.ID_INDICADOR)
	  GROUP BY	dc.ID_INDICADOR 
	)AS C
	ON A.IdIndicador = C.IdIndicador;