	SELECT	bm.ID_INDICADOR AS IdIndicador,
			bn.BANDERA Bandera,
			MAX(FECHA_HORA)::DATE AS FechaBandera,
			bm.ID_PAIS AS Pais
	  FROM	schvtc_enr_sie_monitor.ENR_DET_BITACORAMONITOREO bm
INNER JOIN	schvtc_raw_sie_monitor.RAW_CAT_BANDERAMONITOREO bn
		ON	bm.ID_INDICADOR = bn.ID_INDICADOR AND bm.ID_PAIS = bn.ID_PAIS 
	 WHERE	ID_TIPO = 3
	   AND	FECHA_HORA::DATE = :PdFecha 
	   AND	bn.STATUS = true
  GROUP BY	bm.ID_INDICADOR,
			bn.BANDERA,
			bm.ID_PAIS
	   
