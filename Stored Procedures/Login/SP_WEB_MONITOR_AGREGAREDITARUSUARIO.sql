
CREATE OR REPLACE PROCEDURE schvtc_web_sie_monitor.SP_WEB_MONITOR_AGREGAREDITARUSUARIO
(
	PcNombre VARCHAR(50), 
	PcApellidoPaterno VARCHAR(50), 
	PcApellidoMaterno VARCHAR(50), 
	PcNumEmpleado VARCHAR(10), 
	PcCorreo VARCHAR(50), 
	PcPassword VARCHAR(100), 
	PcImagen VARCHAR(250)
	
) AS $$
DECLARE
	sql_cmd VARCHAR(65000);
	results VARCHAR(65000);
	sql_cmdLog 	VARCHAR(65000);
	resultsLog 	VARCHAR(65000);
	Vibandera	INTEGER;
	ViUsuario	INTEGER;
	VdFecha		DATETIME;
	VdFechaAct	DATETIME;
	VbStatus	BOOLEAN;
	VcEspacio 	VARCHAR(1);
	IDLog 		INTEGER;
	VcProceso	VARCHAR(200);
	VcStoredName VARCHAR(100);
	VcError  	VARCHAR(400);
	message_1 	VARCHAR;
    message_2 	VARCHAR;
    message_3 	VARCHAR;

BEGIN
	----- Asignar los valores para el LOG -----
	VcStoredName := 'SP_WEB_MONITOR_AGREGAREDITARUSUARIO';
	VcError := '';

	RAISE INFO 'Crear o actualizar el usuario: %', PcNumEmpleado;
	
	----- Asignaciones para el proceso -----
	Vibandera := 0;
	VdFecha:= GETDATE();
	VbStatus:= TRUE;
	VcEspacio := ' ';
	
	----- Revisar que el NumEmpleado no venga NULL o vac�o, genera excepcion -----
	IF PcNumEmpleado IS NULL OR PcNumEmpleado = ''
	THEN
		RAISE EXCEPTION 'Se requiere el numero de empleado';
		RETURN;
	END IF;
	
	----- Revisar que el NumEmpleado existe como usuario, de lo contrario se actualiza -----
	IF NOT EXISTS (
					SELECT
						NUM_EMPLEADO
					FROM
						schvtc_raw_sie_monitor.RAW_CAT_USUARIO
					WHERE
						NUM_EMPLEADO = PcNumEmpleado) 
	THEN
		Vibandera := 1;	
	END IF;
	
	----- Crea o actualiza un usuario, genera excepcion -----
	IF Vibandera = 1 THEN
	
		ViUsuario := EXECUTE 'SELECT (MAX(ID_USUARIO) + 1) FROM schvtc_raw_sie_monitor.RAW_CAT_USUARIO';
		RAISE INFO 'Nuevo IdUsuario: %', ViUsuario;
		
		----- Nuevo Usuario -----
		sql_cmd := EXECUTE ' 
							SELECT u.ID_USUARIO ||'',''''''|| u.NOMBRE ||'''''',''''''|| u.APELLIDO_PATERNO ||'''''',''''''|| u.APELLIDO_MATERNO ||'''''',
									''''''|| u.NUM_EMPLEADO ||'''''',''''''|| u.CORREO ||'''''',''''''|| u.PASSWORD ||'''''',
									''''''|| CAST(U.FECHA_CREACION AS VARCHAR(23)) ||'''''',''''''|| u.IMAGEN ||'''''',''''''|| u.STATUS ||''''''''
							FROM (
									SELECT '''|| ViUsuario ||''' AS ID_USUARIO,
									'''|| PcNombre ||''' AS NOMBRE,
								    '''|| PcApellidoPaterno ||''' AS APELLIDO_PATERNO,
								    '''|| PcApellidoMaterno ||''' AS APELLIDO_MATERNO,
								    '''|| PcNumEmpleado ||''' AS NUM_EMPLEADO,
									'''|| PcCorreo || ''' AS CORREO,
									'''|| PcPassword || ''' AS PASSWORD,
									'''|| VdFecha || ''' AS FECHA_CREACION,
									'''|| PcImagen || ''' AS IMAGEN,
									'''|| VbStatus || ''' AS STATUS
								) u;';
							
		results := EXECUTE 'INSERT INTO schvtc_raw_sie_monitor.RAW_CAT_USUARIO (ID_USUARIO, NOMBRE, APELLIDO_PATERNO, APELLIDO_MATERNO, NUM_EMPLEADO, CORREO, PASSWORD, FECHA_CREACION, IMAGEN, STATUS) 
							VALUES (' || sql_cmd || ');';
		RAISE INFO 'results: %', results;
		
		----- Genera excepcion, registro al log interno -----
		IF results = 1
		THEN 
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'Se registro un nuevo usuario, NumEmpleado: '|| PcNumEmpleado ||' Nombre: '|| PcNombre ||' '|| PcApellidoPaterno ||' '|| PcApellidoMaterno ||'';
		ELSE 
			GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
									message_2 = MESSAGE_TEXT,
									message_3 = EXCEPTION_CONTEXT;
								
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
			VcError := 'ERRORSQL: '||message_3 || '';
		
		END IF;
		
		sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
								''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
								FROM (
										SELECT '''|| IDLog ||''' AS ID_LOG,
										'''|| VcProceso ||''' AS PROCESO,
										'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
										'''|| VcError ||''' AS ERRORSQL,
										'''|| VdFecha ||''' AS FECHA_LOG
									 ) a;';
		resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';
	
	ELSE 
		
		VdFechaAct := GETDATE();
		ViUsuario := EXECUTE 'SELECT ID_USUARIO FROM schvtc_raw_sie_monitor.RAW_CAT_USUARIO WHERE NUM_EMPLEADO = '|| PcNumEmpleado ||'';
		RAISE INFO 'Usuario: %', ViUsuario;
		
		----- Se actualiza usuario -----
		sql_cmd := EXECUTE ' 
							UPDATE	schvtc_raw_sie_monitor.RAW_CAT_USUARIO
							  SET	NOMBRE = '''|| PcNombre ||''',
									APELLIDO_PATERNO = '''|| PcApellidoPaterno ||''',
									APELLIDO_MATERNO = '''|| PcApellidoMaterno ||''',
									CORREO = '''|| PcCorreo || ''',
									FECHA_ACTUALIZACION = '''|| VdFechaAct || ''',
									IMAGEN = '''|| PcImagen || ''',
									STATUS = '''|| VbStatus || '''
							WHERE	ID_USUARIO = '''|| ViUsuario ||'''
							  AND	STATUS = TRUE';
		RAISE INFO 'results: %', sql_cmd;
		
		----- Registro al log interno -----
		IF sql_cmd = 1
		THEN 
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'Se actualiza el usuario con NumEmpleado: '|| PcNumEmpleado ||' Nombre: '|| PcNombre ||' '|| PcApellidoPaterno ||' '|| PcApellidoMaterno ||'';
		ELSE 
			GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
									message_2 = MESSAGE_TEXT,
									message_3 = EXCEPTION_CONTEXT;
								
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
			VcError := 'ERRORSQL: '||message_3 || '';
		
		END IF;
	
		sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
								''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
								FROM (
										SELECT '''|| IDLog ||''' AS ID_LOG,
										'''|| VcProceso ||''' AS PROCESO,
										'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
										'''|| VcError ||''' AS ERRORSQL,
										'''|| VdFecha ||''' AS FECHA_LOG
									 ) a;';
		resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';
	
	END IF;
	
	EXCEPTION
		WHEN OTHERS THEN -- OTHERS catches all exceptions
		GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
								message_2 = MESSAGE_TEXT,
								message_3 = EXCEPTION_CONTEXT;
		/*
		 * RAISE INFO 'SQLSTATE: %', message_1;
		 * RAISE INFO 'MESSAGE: %', message_2;
		 * RAISE INFO 'EXCEPTION_CONTEXT: %', message_3;
		*/
		
		IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
		VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
		VcError := 'ERRORSQL: ' || message_3 || '';
		
		sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
								''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
								FROM (
										SELECT '''|| IDLog ||''' AS ID_LOG,
										'''|| VcProceso ||''' AS PROCESO,
										'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
										'''|| VcError ||''' AS ERRORSQL,
										'''|| VdFecha ||''' AS FECHA_LOG
									 ) a;';
		resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';
END;
$$;











