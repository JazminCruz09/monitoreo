
CREATE OR REPLACE PROCEDURE schvtc_web_sie_monitor.SP_WEB_MONITOR_GUDARHORADIE
(
	PiIndicador INTEGER,
	PcHoraDIE	VARCHAR(5),
	PiUsuario	INTEGER,
	PiRol		INTEGER
)
AS $$
DECLARE 
		sql_cmd 	VARCHAR(65000);
		results 	VARCHAR(65000);
		sql_cmdLog 	VARCHAR(65000);
		resultsLog 	VARCHAR(65000);
		excepcion 	VARCHAR(65000);
		VdFecha 	DATETIME;
		ViBandera 	INTEGER;
		VcUser		VARCHAR(50);
		VcIndicador	VARCHAR(50);
		ViPermiso	INTEGER;
		VcEspacio 	VARCHAR(1);
		ViAcceso 	INTEGER;
		IDLog 		INTEGER;
		VcProceso	VARCHAR(200);
		VcStoredName VARCHAR(100);
		VcError  	VARCHAR(400);
		message_1	VARCHAR;
		message_2	VARCHAR;
		message_3	VARCHAR;
BEGIN 
	
		----- Asignar los valores para el LOG -----
		VcStoredName := 'SP_WEB_MONITOR_GUDARHORADIE';
		VcError := '';
		
		VdFecha := GETDATE();
		VcEspacio := ' ';
		ViBandera := 0;
		VcUser := EXECUTE 'SELECT NOMBRE ||'''|| VcEspacio ||'''|| APELLIDO_PATERNO FROM schvtc_raw_sie_monitor.RAW_CAT_USUARIO WHERE ID_USUARIO = '|| PiUsuario ;
		VcIndicador := EXECUTE 'SELECT INDICADOR FROM schvtc_raw_sie_monitor.RAW_CAT_INDICADORMONITOREO WHERE ID_INDICADOR = '|| PiIndicador ;
		
		----- Obtener si el usuario asignado el perfil -----
		ViAcceso := EXECUTE 'SELECT	COUNT(1) 
							   FROM	schvtc_raw_sie_monitor.RAW_DET_PERFILAMIENTO 
							  WHERE	ID_USUARIO = '|| PiUsuario ||' 
							 	AND	ID_ROL = '|| PiRol ||'
							 	AND	ID_ACCESO = 1 
							 	AND	(AGREGAR = TRUE OR EDITAR = TRUE)
							 	AND	ESTATUS = TRUE';
		
		----- Obtener si el usuario tiene permiso al indicador -----
		ViPermiso := EXECUTE 'SELECT COUNT(1) 
							    FROM schvtc_raw_sie_monitor.RAW_DET_PERFILINDICADOR 
							   WHERE ID_USUARIO = '|| PiUsuario ||' 
							  	 AND ID_ROL = '|| PiRol ||'
							  	 AND ID_ACCESO = 1 
							  	 AND ID_INDICADOR = '|| PiIndicador ||'';
		
		----- Asignar la bandera si el usuario puede o no, registrar la hora DIE -----
		IF (ViAcceso = 1 AND ViPermiso = 1) THEN 
			ViBandera := 1;
			
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'El usuario '||VcUser||' tiene permisos para resgistrar la HoraDIe';
			RAISE INFO '%', VcProceso;
		ELSE
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'El usuario '||VcUser||' intento resgistrar la HoraDIE del indicador '||PiIndicador ||' pero no tiene permisos';
			RAISE INFO '%', VcProceso;
		END IF;
		
		sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFecha ||''' AS FECHA_LOG
										 ) a;';
		resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES ('|| sql_cmdLog ||');';
	
		IF ViBandera = 1 THEN
			----- Registro de HORADIE -----
			sql_cmd := EXECUTE 'SELECT h.ID_INDICADOR ||'',''''''|| CAST(h.HORA_DIE AS VARCHAR(5)) ||'''''',''''''|| CAST(h.FECHA_DIE AS VARCHAR(23)) ||'''''',''|| h.ID_USUARIO 
								FROM (
										SELECT '''|| PiIndicador ||''' AS ID_INDICADOR,
												'''|| PcHoraDIE ||''' AS HORA_DIE,
												'''|| VdFecha ||''' AS FECHA_DIE,
												'''|| PiUsuario ||''' AS ID_USUARIO
									) h;';
			results := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_DET_LIBERACIONDIE VALUES ('|| sql_cmd ||');';
			RAISE INFO 'Results %', results;
			
			----- Registro al log interno -----
			IF results = 1 THEN 
				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcProceso := 'Se registro la HoraDIE del Indicador '|| PiIndicador ||', realizado por '||VcUser||'';
			ELSE
				GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
										message_2 = MESSAGE_TEXT,
										message_3 = EXCEPTION_CONTEXT;
			
				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
				VcError := 'ERRORSQL: ' || message_3 || '';
			END IF;
			
			sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFecha ||''' AS FECHA_LOG
										 ) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';
		
		END IF;
		
		EXCEPTION
			WHEN OTHERS THEN -- OTHERS catches all exceptions
			GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
									message_2 = MESSAGE_TEXT,
									message_3 = EXCEPTION_CONTEXT;
			
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
			VcError := 'ERRORSQL: ' || message_3 || '';
			
			sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFecha ||''' AS FECHA_LOG
										 ) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';

END;
$$;




