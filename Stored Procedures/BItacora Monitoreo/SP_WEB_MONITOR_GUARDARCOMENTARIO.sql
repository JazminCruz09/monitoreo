
CREATE OR REPLACE PROCEDURE schvtc_web_sie_monitor.SP_WEB_MONITOR_GUARDARCOMENTARIO
(
	PiIndicador INTEGER,
	PiUsuario	INTEGER,
	PcComentario VARCHAR(200)
)
AS 
$$
DECLARE 
	sql_cmd 	VARCHAR(65000);
	results 	VARCHAR(65000);
	sql_cmdLog 	VARCHAR(65000);
	resultsLog 	VARCHAR(65000);
	VdFecha 	DATETIME;
	ViAcceso 	INTEGER;
	VcUser		VARCHAR(50);
	ViBandera	INTEGER;
	Espacio		VARCHAR(1);
	IDLog 		INTEGER;
	VcProceso	VARCHAR(200);
	VcStoredName  VARCHAR(100);
	VcError  	VARCHAR(400);
	message_1 	VARCHAR;
    message_2 	VARCHAR;
    message_3 	VARCHAR;
		
BEGIN 
		Espacio := ' ';
		ViBandera := 0;
	
		----- Asignar los valores para el LOG -----
		VcStoredName := 'SP_WEB_MONITOR_GUARDARCOMENTARIO';
		VcError := '';
		
		----- Saber si el usuario tiene permiso en el indicador -----
		ViAcceso := EXECUTE 'SELECT ISNULL(COUNT(ID_ACCESO), 0) FROM schvtc_raw_sie_monitor.RAW_DET_PERFILINDICADOR WHERE ID_INDICADOR = '||PiIndicador||' AND ID_USUARIO = '||PiUsuario||'';
		VcUser := EXECUTE 'SELECT NOMBRE ||'''|| Espacio ||'''||APELLIDO_PATERNO FROM schvtc_raw_sie_monitor.RAW_CAT_USUARIO WHERE ID_USUARIO = '||PiUsuario||'';
		VdFecha := GETDATE();
		
		----- Saber el usuario tiene acceso, asigna bandera e inserta en el Log -----
		IF ViAcceso = 1 THEN
			ViBandera := 1;
			
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'El usuario '||VcUser||' tiene permisos para resgistrar comentarios del indicador '||PiIndicador||'';		
		ELSE
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'El usuario '||VcUser||' intento resgistrar comentarios del indicador '||PiIndicador||' pero no tiene permiso';
		END IF;
		RAISE INFO 'BANDERA: %', ViBandera;
		
		----- Registro al log interno -----
		sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFecha ||''' AS FECHA_LOG
										 ) a;';
		resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES ('|| sql_cmdLog ||');';
		
		IF ViBandera = 1 THEN 
			----- Registro de comentarios -----
			sql_cmd = EXECUTE '
								SELECT c.ID_INDICADOR ||'',''|| c.ID_USUARIO ||'',''''''||CAST(c.FECHA AS VARCHAR(23))||'''''',''''''|| c.COMENTARIO ||''''''''
								FROM (
										SELECT '''|| PiIndicador ||''' AS ID_INDICADOR,
												'''|| PiUsuario ||''' AS ID_USUARIO,
												'''|| VdFecha ||''' AS FECHA,
												'''|| PcComentario ||''' AS COMENTARIO
									 ) c;';
									
			results := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_DET_COMENTARIOSMONITOREO VALUES (' || sql_cmd ||');';
			RAISE INFO 'Results: %', results;
			
			----- Registro al log interno -----
			IF results = 1 THEN 
				
				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcProceso := 'El usuario '||PiUsuario||' registro el comentario exitosamente';
			ELSE 
				GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
										message_2 = MESSAGE_TEXT,
										message_3 = EXCEPTION_CONTEXT;		
									
				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
				VcError := 'ERRORSQL: '||message_3 || '';
			END IF;
		
			sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
										''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
										FROM (
												SELECT '''|| IDLog ||''' AS ID_LOG,
												'''|| VcProceso ||''' AS PROCESO,
												'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
												'''|| VcError ||''' AS ERRORSQL,
												'''|| VdFecha ||''' AS FECHA_LOG
											 ) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES ('|| sql_cmdLog ||');';
		
		END IF;
	
		EXCEPTION
			WHEN OTHERS THEN -- OTHERS catches all exceptions
			GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
									message_2 = MESSAGE_TEXT,
									message_3 = EXCEPTION_CONTEXT;
			
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
			VcError := 'ERRORSQL: ' || message_3 || '';
			
			sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFecha ||''' AS FECHA_LOG
										 ) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES ('|| sql_cmdLog ||');';

END
$$;






