
CREATE OR REPLACE PROCEDURE schvtc_raw_repo.SP_RAW_MONITOR_BITACORAMONITOREO
(
	PiIndicador INTEGER, 
	PiTipo 		INTEGER, 
	PdFecha 	DATETIME, 
	PiSemana 	INTEGER, 
	PiPais 		INTEGER
) AS $$
DECLARE
	sql_cmd 	VARCHAR(65000);
	results 	VARCHAR(65000);
	sql_cmdLog 	VARCHAR(65000);
	resultsLog 	VARCHAR(65000);
	VcTipoProceso VARCHAR(20);
	ViBanderaP	INTEGER;
	ViBanderaT	INTEGER;
	IDLog 		INTEGER;
	VcProceso	VARCHAR(200);
	VcStoredName  VARCHAR(100);
	VcError		VARCHAR(400);
	VdFechaLog	DATETIME;
	message_1 	VARCHAR;
    message_2 	VARCHAR;
    message_3 	VARCHAR;

BEGIN

		----- Asignar los valores para el LOG -----
		VcStoredName := 'SP_RAW_MONITOR_BITACORAMONITOREO';
		VdFechaLog := GETDATE();
		VcError := '';
		
		----- Validar que los parametros Indicador, Tipo, Pais no sean NULL o 0 -----
		IF	PiIndicador IS NULL OR PiIndicador = 0 OR 
			PiTipo IS NULL OR PiTipo = 0 OR 
			PiPais IS NULL OR PiPais = 0 
		THEN
			RAISE EXCEPTION 'Se requiere IdIndicador, TipoProceso o Pais';
			RETURN;
		ELSE 
			ViBanderaP := 1;
		END IF;  
		
		----- Validar el tipo de proceso BANDERA se contemple el n�mero de semana -----
		IF	PiTipo <> 3 AND PiSemana <> 0
		THEN
			RAISE EXCEPTION 'El tipo de proceso a registrar no requiere numero de semana';
			RETURN;
		ELSE
			ViBanderaT := 1;
		END IF;
		
		----- Generar el registro en la bitacora -----
		IF	(ViBanderaP = 1) AND (ViBanderaT = 1)
		THEN
			----- Registra las horas de liberaci�n de los procesos CARGA, CAPA, BANDERA -----
			sql_cmd := EXECUTE ' 
								SELECT c.ID_INDICADOR ||'',''|| c.ID_TIPO ||'',''''''||CAST(c.FECHA AS VARCHAR(19)) 
										||'''''','' || c.SEMANA ||'','' || c.ID_PAIS 
								FROM (
										SELECT	'''|| PiIndicador ||''' AS ID_INDICADOR,
										'''|| PiTipo ||''' AS ID_TIPO,
										'''|| PdFecha ||''' AS FECHA,
										'''|| PiSemana ||''' AS SEMANA,
										'''|| PiPais ||''' AS ID_PAIS
									) c;';		
				
			results := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_DET_BITACORAMONITOREO VALUES (' || sql_cmd ||');';
			RAISE INFO 'Results: %', results;
			
			----- Registro al log interno -----
			IF results = 1
			THEN 
				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcTipoProceso := EXECUTE 'SELECT TIPO_PROCESO FROM schvtc_raw_sie_monitor.RAW_CAT_TIPOPROCESO WHERE ID_TIPO = '||PiTipo||'';
				VcProceso := 'Se registra el indicador '||PiIndicador||' con el proceso '||PiTipo||'-'||VcTipoProceso||'';
				
				RAISE INFO 'Results: %', VcProceso;
			ELSE 
				GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
									message_2 = MESSAGE_TEXT,
									message_3 = EXCEPTION_CONTEXT;
								
				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
				VcError := 'ERRORSQL: '||message_3 || '';
			END IF;
			
			sql_cmdLog := EXECUTE ' 
									SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||'''''''' 
									FROM (
											SELECT	'''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFechaLog ||''' AS FECHA_LOG
										) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';
		END IF;
	
		EXCEPTION	
			WHEN OTHERS THEN -- OTHERS catches all exceptions
			GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
									message_2 = MESSAGE_TEXT,
									message_3 = EXCEPTION_CONTEXT;
								
			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
			VcError := 'ERRORSQL: ' || message_3 || '';
			
			sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFechaLog ||''' AS FECHA_LOG
										 ) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';

END;
$$;





