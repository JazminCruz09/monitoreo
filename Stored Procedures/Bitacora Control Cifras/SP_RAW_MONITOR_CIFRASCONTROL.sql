
CREATE OR REPLACE PROCEDURE schvtc_raw_repo.SP_RAW_MONITOR_CIFRASCONTROL
(
	PiDetalle INTEGER, PnValor DOUBLE PRECISION, PdFecha DATETIME, PcComentario VARCHAR(512), PdFechaV  VARCHAR(10)
) AS $$
DECLARE
	sql_cmd 	VARCHAR(65000);
	results 	VARCHAR(65000);
	sql_cmdLog 	VARCHAR(65000);
	resultsLog 	VARCHAR(65000);
	VdFechaV 	VARCHAR(10);
	ViBandera	INTEGER;
	IDLog 		INTEGER;
	VcProceso 	VARCHAR(200);
	VcStoredName VARCHAR(100);
	VdFechaLog	DATETIME;
	VcError 	VARCHAR(400);
	message_1 	VARCHAR;
    message_2 	VARCHAR;
    message_3 	VARCHAR;

BEGIN
		----- Asignar los valores para el log -----
		VcStoredName := 'SP_RAW_MONITOR_CIFRASCONTROL';
		VdFechaLog := GETDATE();
		VcError := '';
		
		IF	
			PiDetalle IS NULL OR PiDetalle = 0 
		THEN
	  		RAISE EXCEPTION 'Se requiere IdDetalle';
	        RETURN;
	    END IF;
	   
		IF PdFechaV IS NULL 
		THEN 
			VdFechaV:= '';
		END IF;
		
		IF  NOT EXISTS (SELECT ID_DETALLE_CIFRA FROM schvtc_enr_sie_monitor.ENR_DET_DETALLECIFRA WHERE ID_DETALLE_CIFRA = PiDetalle) THEN
   			RAISE EXCEPTION 'Coherencia de registros inexistente.';
   			RETURN; 
   		ELSE
   			ViBandera := 1;
   		END IF;
   		RAISE INFO 'Bandera %', ViBandera;
   		
   		IF ViBandera = 1 THEN 
   			----- Registro a la bitacora de control cifras. -----
	   		sql_cmd := EXECUTE ' 
								SELECT c.ID_DETALLE_CIFRA ||'',''|| c.TOTAL_CIFRA ||'',''''''|| CAST(c.FECHA AS VARCHAR(23)) ||''''''
								,''''''|| c.NOTAS ||'''''',''''''|| c.FECHAV ||''''''''
								FROM (
										SELECT '''|| PiDetalle ||''' AS ID_DETALLE_CIFRA,
								      	'''|| PnValor ||''' AS TOTAL_CIFRA,
								      	'''|| PdFecha ||''' AS FECHA,
								      	'''|| PcComentario ||''' AS NOTAS,
										'''|| VdFechaV || ''' AS FECHAV
									) c;';		
		
			results := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS VALUES ('|| sql_cmd ||');';
			RAISE INFO 'Results: %', results;
		
			IF results = 1 THEN 
				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcProceso := 'Se registra en schvtc_enr_sie_monitor.ENR_DET_CONTROLCIFRAS el IdDetalle '|| PiDetalle ||' con el valor '|| PnValor ||'';
			
			ELSE 
				GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
										message_2 = MESSAGE_TEXT,
										message_3 = EXCEPTION_CONTEXT;

				IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
				VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
				VcError := 'ERRORSQL: ' || message_3 || '';
			
			END IF;
			
			----- Registro al log. -----
			sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFechaLog ||''' AS FECHA_LOG
										 ) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';
   		END IF;
   
   		EXCEPTION
			WHEN OTHERS THEN -- OTHERS catches all exceptions
			GET STACKED DIAGNOSTICS message_1 = RETURNED_SQLSTATE,
									message_2 = MESSAGE_TEXT,
									message_3 = EXCEPTION_CONTEXT;

			IDLog := EXECUTE 'SELECT ISNULL(MAX(ID_LOG), 0) + 1 FROM schvtc_enr_sie_monitor.ENR_LOG_MONITOREO';
			VcProceso := 'SQLSTATE: '|| message_1 ||' MESSAGE: '|| message_2 ||'';
			VcError := 'ERRORSQL: ' || message_3 || '';
			
			sql_cmdLog := EXECUTE '	SELECT a.ID_LOG ||'',''''''|| a.PROCESO ||'''''',''''''|| a.NOMBRE_STOREPROCEDURE ||'''''',''''''|| a.ERRORSQL ||'''''',
									''''''||CAST(a.FECHA_LOG AS VARCHAR(23))||''''''''
									FROM (
											SELECT '''|| IDLog ||''' AS ID_LOG,
											'''|| VcProceso ||''' AS PROCESO,
											'''|| VcStoredName ||''' AS NOMBRE_STOREPROCEDURE,
											'''|| VcError ||''' AS ERRORSQL,
											'''|| VdFechaLog ||''' AS FECHA_LOG
										 ) a;';
			resultsLog := EXECUTE 'INSERT INTO schvtc_enr_sie_monitor.ENR_LOG_MONITOREO VALUES (' || sql_cmdLog ||');';
	
END;
$$;






